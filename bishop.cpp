/* Copyright (c) (2012) (Gauthier Brion) Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include "bishop.h"

#include "board.h"

Bishop::Bishop(Piece::Color color, const Board &board, QDeclarativeItem *square) :
    Piece(Piece::Bishop, color, board, square)
{

}

QStringList Bishop::getPathTo(const QString &)
{
    return QStringList();
}

QStringList Bishop::foundAllowedDestinations() const
{
    QStringList allowedDestinations;
    const QString squareName = getMySquareName();
    const char row = squareName.at(1).toLatin1();
    const char col = squareName.at(0).toLatin1();
    const QVector<QPoint> directions = {QPoint(1,1), QPoint(-1,1), QPoint(-1,-1), QPoint(1,-1)};

    QString destinationSquareName;
    Piece *destinationPiece;

    // Search allowed squares in each four directions
    foreach (const QPoint &direction, directions)
    {
        int i = col + direction.x();
        int j = row + direction.y();
        while (i >= 'a' && i <= 'h' && j >= '1' && j <= '8')
        {
            destinationSquareName.sprintf("%c%c", i, j);
            destinationPiece = m_board.getPiece(destinationSquareName);

            // If the square is occupied, we can't go further in that direction.
            // We only need to know if we can go to this last square or not.
            if (destinationPiece)
            {
                // If it's an opponent piece, we can move to this last square
                if (destinationPiece->getColor() != getColor())
                    allowedDestinations << destinationSquareName;

                break;
            }

            allowedDestinations << destinationSquareName;
            i += direction.x();
            j += direction.y();
        }
    }

    return allowedDestinations;
}
