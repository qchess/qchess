/* Copyright (c) (2012) (Gauthier Brion) Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include "piece.h"

#include "board.h"
#include <QDeclarativeComponent>
#include <QDeclarativeItem>

Piece::Piece(Type type, Color color, const Board &board, QDeclarativeItem *square) :
    QObject(),
    m_calculatedMoveCount(-1),
    m_calculatedAllowedDestinations(QStringList()),
    m_type(type),
    m_color(color),
    m_moveCount(0),
    m_declarativeItem(NULL),
    m_board(board)
{
    QString imagePath;

    switch (color)
    {
    case White:
        imagePath = "images/white/";
        break;
    case Black:
        imagePath = "images/black/";
        break;
    default:
        return; //TODO Warning for bad color
    }

    switch (type)
    {
    case Pawn:
        imagePath += "pawn";
        break;
    case Rook:
        imagePath += "rook";
        break;
    case Knight:
        imagePath += "knight";
        break;
    case Bishop:
        imagePath += "bishop";
        break;
    case Queen:
        imagePath += "queen";
        break;
    case King:
        imagePath += "king";
        break;
    default:
        return; //TODO Warning for bad type
    }

    imagePath += ".png";

    QDeclarativeComponent component(m_board.engine(), QUrl("qrc:/piece.qml"));
    m_declarativeItem = qobject_cast<QDeclarativeItem *>(component.create(m_board.rootContext()));
    m_declarativeItem->setParentItem(square);
    m_declarativeItem->setProperty("width", "parent.width");
    m_declarativeItem->setProperty("height", "parent.height");
    m_declarativeItem->setProperty("source", QUrl("qrc:/" + imagePath));

    connect(m_declarativeItem, SIGNAL(clicked()), this, SIGNAL(clicked()));
}

Piece::~Piece()
{
    delete m_declarativeItem;
}

void Piece::setSelectedState(bool isSelected)
{
    if (isSelected)
        m_declarativeItem->setProperty("state", "selected state");
    else
        m_declarativeItem->setProperty("state", "base state");
}

QStringList Piece::getAllowedDestinations() const
{
    if (m_board.getMoveCount() == m_calculatedMoveCount)
        return m_calculatedAllowedDestinations;

    m_calculatedMoveCount = m_board.getMoveCount();
    m_calculatedAllowedDestinations = foundAllowedDestinations();
    return m_calculatedAllowedDestinations;
}

void Piece::move(QDeclarativeItem *square)
{
    ++m_moveCount;
    m_declarativeItem->setParentItem(square);
}

Piece::Type Piece::getType() const
{
    return m_type;
}

Piece::Color Piece::getColor() const
{
    return m_color;
}

QString Piece::getMySquareName() const
{
    return m_declarativeItem->parentItem()->objectName();
}

QDeclarativeItem *Piece::getDeclarativeItem() const
{
    return m_declarativeItem;
}

int Piece::getMoveCount() const
{
    return m_moveCount;
}
