/* Copyright (c) (2012) (Gauthier Brion) Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "board.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);    

    m_board = new Board(this);
    setCentralWidget(m_board);
    m_board->setResizeMode(QDeclarativeView::SizeRootObjectToView);
    m_board->resize(660, 660);
    m_board->show();

    connect(ui->actionStartNewGame, SIGNAL(triggered()), m_board, SLOT(newGame()));
    connect(m_board, SIGNAL(check()), this, SLOT(slotCheckOccured()));
    connect(m_board, SIGNAL(checkMate()), this, SLOT(slotCheckmateOccured()));
    connect(m_board, SIGNAL(whiteMoveEnded()), this, SLOT(slotClearMessage()));
    connect(m_board, SIGNAL(blackMoveEnded()), this, SLOT(slotClearMessage()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::slotCheckOccured()
{
    ui->informationL->setText("Check");
}

void MainWindow::slotCheckmateOccured()
{
    ui->informationL->setText("Checkmate");
}

void MainWindow::slotSlatemateOccured()
{
    ui->informationL->setText("Slatemate");
}

void MainWindow::slotClearMessage()
{
    ui->informationL->setText("");
}
/*
void MainWindow::slotStartLightTimer()
{
    m_lightTimer.start();
}

void MainWindow::slotPauseLightTimer()
{
    m_lightTimer.stop();
}

void MainWindow::slotStartDarkTimer()
{
    m_darkTimer.start();
}

void MainWindow::slotPauseDarkTimer()
{
    m_darkTimer.stop();
}
*/
