/* Copyright (c) (2012) (Gauthier Brion) Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include "pawn.h"

#include "board.h"

Pawn::Pawn(Piece::Color color, const Board &board, QDeclarativeItem *square) :
    Piece(Piece::Pawn, color, board, square)
{
}

QStringList Pawn::getPathTo(const QString &squareName)
{
    if (!getAllowedDestinations().contains(squareName))
        return QStringList();

    const char rowFrom = getMySquareName().at(1).toLatin1();
    const char colFrom = getMySquareName().at(0).toLatin1();
    const char rowTo = squareName.at(1).toLatin1();
    const char colTo = squareName.at(0).toLatin1();

    QStringList path;
    if (colFrom != colTo)
        path << squareName;
    else
    {
        if (abs(rowFrom - rowTo) == 1)
            path << squareName;
        else
        {
            foreach (const QString &destination, getAllowedDestinations())
                if (destination.at(0).toLatin1() == colTo)
                    path << destination;
        }
    }

    return path;
}

QStringList Pawn::foundAllowedDestinations() const
{
    QStringList allowedDestinations;
    const QString squareName = getMySquareName();
    const char row = squareName.at(1).toLatin1();
    const char col = squareName.at(0).toLatin1();
    const int direction = (getColor() == Piece::White)?1:-1;

    QString destinationSquareName;
    Piece *destinationPiece;

    // Normal move (1 square in front)
    destinationSquareName.sprintf("%c%c", col, row + direction);
    destinationPiece = m_board.getPiece(destinationSquareName);
    if (!destinationPiece)
        allowedDestinations << destinationSquareName;

    // Special move (2 squares in front, if not yet moved and if nothing is on the first square)
    if (!destinationPiece && isFirstMove())
    {
        destinationSquareName.sprintf("%c%c", col, row + 2 * direction);
        destinationPiece = m_board.getPiece(destinationSquareName);
        if (!destinationPiece)
            allowedDestinations << destinationSquareName;
    }

    // Capturing opponent piece move (left & right)
    destinationSquareName.sprintf("%c%c", col - 1, row + direction);
    destinationPiece = m_board.getPiece(destinationSquareName);
    if (destinationPiece && destinationPiece->getColor() != getColor())
        allowedDestinations << destinationSquareName;
    destinationSquareName.sprintf("%c%c", col + 1, row + direction);
    destinationPiece = m_board.getPiece(destinationSquareName);
    if (destinationPiece && destinationPiece->getColor() != getColor())
        allowedDestinations << destinationSquareName;

    // TODO Add the "En passant" square if allowed

    return allowedDestinations;
}

bool Pawn::isFirstMove() const
{
    return m_moveCount == 0;
}
