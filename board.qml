/* Copyright (c) (2012) (Gauthier Brion) Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import QtQuick 1.1

Rectangle {
    id: board
    width: 860
    height: 860
    color: "#394977"

    signal squareClicked(string squareName)

    property int labelThickness: 30
    property color darkColor: "#8694d9"
    property color lightColor: "white"
    property color textColor: "white"

    Grid {
        anchors.fill: parent
        rows: 10
        columns: 10

        property int squareWidth: (width - 2*labelThickness) / 8
        property int squareHeight: (height - 2*labelThickness) / 8


        Item {
            width: board.labelThickness
            height: board.labelThickness
        }

        Item {
            width: parent.squareWidth
            height: board.labelThickness
        }

        Item {
            width: parent.squareWidth
            height: board.labelThickness
        }

        Item {
            width: parent.squareWidth
            height: board.labelThickness
        }

        Item {
            width: parent.squareWidth
            height: board.labelThickness
        }

        Item {
            width: parent.squareWidth
            height: board.labelThickness
        }

        Item {
            width: parent.squareWidth
            height: board.labelThickness
        }

        Item {
            width: parent.squareWidth
            height: board.labelThickness
        }

        Item {
            width: parent.squareWidth
            height: board.labelThickness
        }

        Item {
            width: board.labelThickness
            height: board.labelThickness
        }

        Text {
            text: "8"
            width: board.labelThickness
            height: parent.squareHeight
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Rectangle {
            id: a8
            objectName: "a8"
            width: parent.squareWidth
            height: parent.squareHeight
            color: "#f7f7f7"

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: b8
            objectName: "b8"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: c8
            objectName: "c8"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: d8
            objectName: "d8"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: e8
            objectName: "e8"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: f8
            objectName: "f8"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: g8
            objectName: "g8"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: h8
            objectName: "h8"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Item {
            width: board.labelThickness
            height: parent.squareHeight
        }

        Text {
            text: "7"
            width: board.labelThickness
            height: parent.squareHeight
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Rectangle {
            id: a7
            objectName: "a7"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: b7
            objectName: "b7"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: c7
            objectName: "c7"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: d7
            objectName: "d7"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: e7
            objectName: "e7"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: f7
            objectName: "f7"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: g7
            objectName: "g7"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: h7
            objectName: "h7"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Item {
            width: board.labelThickness
            height: parent.squareHeight
        }

        Text {
            text: "6"
            width: board.labelThickness
            height: parent.squareHeight
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Rectangle {
            id: a6
            objectName: "a6"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: b6
            objectName: "b6"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: c6
            objectName: "c6"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: d6
            objectName: "d6"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: e6
            objectName: "e6"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: f6
            objectName: "f6"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: g6
            objectName: "g6"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: h6
            objectName: "h6"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Item {
            width: board.labelThickness
            height: parent.squareHeight
        }

        Text {
            text: "5"
            width: board.labelThickness
            height: parent.squareHeight
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Rectangle {
            id: a5
            objectName: "a5"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: b5
            objectName: "b5"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: c5
            objectName: "c5"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: d5
            objectName: "d5"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: e5
            objectName: "e5"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: f5
            objectName: "f5"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: g5
            objectName: "g5"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: h5
            objectName: "h5"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Item {
            width: board.labelThickness
            height: parent.squareHeight
        }

        Text {
            text: "4"
            width: board.labelThickness
            height: parent.squareHeight
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Rectangle {
            id: a4
            objectName: "a4"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: b4
            objectName: "b4"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: c4
            objectName: "c4"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: d4
            objectName: "d4"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: e4
            objectName: "e4"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: f4
            objectName: "f4"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: g4
            objectName: "g4"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: h4
            objectName: "h4"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Item {
            width: board.labelThickness
            height: parent.squareHeight
        }

        Text {
            text: "3"
            width: board.labelThickness
            height: parent.squareHeight
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Rectangle {
            id: a3
            objectName: "a3"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: b3
            objectName: "b3"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: c3
            objectName: "c3"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: d3
            objectName: "d3"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: e3
            objectName: "e3"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: f3
            objectName: "f3"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: g3
            objectName: "g3"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: h3
            objectName: "h3"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Item {
            width: board.labelThickness
            height: parent.squareHeight
        }

        Text {
            text: "2"
            width: board.labelThickness
            height: parent.squareHeight
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Rectangle {
            id: a2
            objectName: "a2"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: b2
            objectName: "b2"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: c2
            objectName: "c2"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: d2
            objectName: "d2"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: e2
            objectName: "e2"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: f2
            objectName: "f2"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: g2
            objectName: "g2"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: h2
            objectName: "h2"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Item {
            width: board.labelThickness
            height: parent.squareHeight
        }

        Text {
            text: "1"
            width: board.labelThickness
            height: parent.squareHeight
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Rectangle {
            id: a1
            objectName: "a1"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: b1
            objectName: "b1"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: c1
            objectName: "c1"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: d1
            objectName: "d1"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: e1
            objectName: "e1"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: f1
            objectName: "f1"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: g1
            objectName: "g1"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.darkColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Rectangle {
            id: h1
            objectName: "h1"
            width: parent.squareWidth
            height: parent.squareHeight
            color: board.lightColor

            MouseArea {
                anchors.fill: parent
                onClicked: board.squareClicked(parent.objectName);
            }
        }

        Item {
            width: board.labelThickness
            height: parent.squareHeight
        }

        Item {
            width: board.labelThickness
            height: board.labelThickness
        }

        Text {
            text: "a"
            width: parent.squareWidth
            height: board.labelThickness
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Text {
            text: "b"
            width: parent.squareWidth
            height: board.labelThickness
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Text {
            text: "c"
            width: parent.squareWidth
            height: board.labelThickness
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Text {
            text: "d"
            width: parent.squareWidth
            height: board.labelThickness
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Text {
            text: "e"
            width: parent.squareWidth
            height: board.labelThickness
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Text {
            text: "f"
            width: parent.squareWidth
            height: board.labelThickness
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Text {
            text: "g"
            width: parent.squareWidth
            height: board.labelThickness
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Text {
            text: "h"
            width: parent.squareWidth
            height: board.labelThickness
            font.pointSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: board.textColor
        }

        Item {
            width: board.labelThickness
            height: board.labelThickness
        }
    }
}
