/* Copyright (c) (2012) (Gauthier Brion) Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include "king.h"

#include "board.h"
#include "player.h"

King::King(Piece::Color color, const Board &board, QDeclarativeItem *square) :
    Piece(Piece::King, color, board, square)
{
    board.getPlayer(color)->setKing(this);
}

QStringList King::getPathTo(const QString &)
{
    return QStringList();
}

QStringList King::foundAllowedDestinations() const
{
    QStringList allowedDestinations;
    const QString squareName = getMySquareName();
    const char row = squareName.at(1).toLatin1();
    const char col = squareName.at(0).toLatin1();
    const QVector<QPoint> directions = {QPoint(1,0), QPoint(1,1), QPoint(0,1), QPoint(-1,1), QPoint(-1,0), QPoint(-1,-1), QPoint(0,-1), QPoint(1,-1)};

    QString destinationSquareName;
    Piece *destinationPiece;

    // Search allowed squares in each eight directions
    foreach (const QPoint &direction, directions)
    {
        const int i = col + direction.x();
        const int j = row + direction.y();
        if (i >= 'a' && i <= 'h' && j >= '1' && j <= '8')
        {
            destinationSquareName.sprintf("%c%c", i, j);
            destinationPiece = m_board.getPiece(destinationSquareName);
            if (destinationPiece && destinationPiece->getColor() == getColor()) continue;

            allowedDestinations << destinationSquareName;
        }
    }

    if (m_moveCount == 0)
    {
        // Search if long castling is allowed
        Piece *longCastlingRook = m_board.getPiece(m_color == White?"a1":"a8");
        if (longCastlingRook && longCastlingRook->getType() == Rook && longCastlingRook->getMoveCount() == 0 &&    // Check if the rook or the king has already moved
                !m_board.getPiece(m_color == White?"b1":"b8") &&    // Check if the square between the rook and the king are empty
                !m_board.getPiece(m_color == White?"c1":"c8") &&
                !m_board.getPiece(m_color == White?"d1":"d8") &&
                m_board.getPlayer(getColor())->isCheck() == NULL &&        // Check if the king is not currently check
                !m_board.getPlayer(getColor())->isSquareControledByOpponent(White?"d1":"d8")    // Check if the king does not pass through controlled square
                )
            allowedDestinations << (m_color == White?"c1":"c8");

        // Search if short castling is allowed
        Piece *shortCastlingRook = m_board.getPiece(m_color == White?"h1":"h8");
        if (shortCastlingRook && shortCastlingRook->getType() == Rook && shortCastlingRook->getMoveCount() == 0 &&    // Check if the rook or the king has already moved
                !m_board.getPiece(m_color == White?"f1":"f8") &&    // Check if the square between the rook and the king are empty
                !m_board.getPiece(m_color == White?"g1":"g8") &&
                m_board.getPlayer(getColor())->isCheck() == NULL &&        // Check if the king is not currently check
                !m_board.getPlayer(getColor())->isSquareControledByOpponent(White?"g1":"g8")    // Check if the king does not pass through controlled square
                )
            allowedDestinations << (m_color == White?"g1":"g8");
    }

    return allowedDestinations;
}
