/* Copyright (c) (2012) (Gauthier Brion) Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include "player.h"

#include "board.h"

Player::Player(Piece::Color color, const Board &board, QObject *parent) :
    QObject(parent),
    m_checkCount(0),
    m_color(color),
    m_board(board),
    m_lastIsCheckMoveCount(-1),
    m_lastIsCheckResult(NULL)
{
}

void Player::clear()
{
    m_checkCount = 0;
}

int Player::getCheckNumber() const
{
    return m_checkCount;
}

Piece * Player::isSquareControledByOpponent(const QString &squareName) const
{
    QList<Piece *> opponentPieces = m_board.getPieces(m_color==Piece::White?Piece::Black:Piece::White); // TODO Store an updated list of player pieces in the Player class?

    foreach (Piece *piece, opponentPieces)
        if (piece->getAllowedDestinations().contains(squareName))
            return piece;

    return NULL;
}

Piece * Player::isCheck() const
{
    if (m_lastIsCheckMoveCount == m_board.getMoveCount())
        return m_lastIsCheckResult;

    m_lastIsCheckMoveCount = m_board.getMoveCount();
    m_lastIsCheckResult = isSquareControledByOpponent(m_king->getMySquareName());

    return m_lastIsCheckResult;
}

bool Player::isStuck() const
{
    if (isCheck() != NULL)
    {
        // Check if the king can move to a square which is not controled by opponent pieces
        QStringList kingAllowedDestinations = m_king->getAllowedDestinations();
        foreach (const QString &square, kingAllowedDestinations)
            if (!isSquareControledByOpponent(square))
                return false;

        // Check if a piece can move in between the King and the enemy piece to block the check
        //QStringList squaresBetween = m_lastIsCheckResult->getPathTo(m_king->getMySquareName()).removeAll(m_king->getMySquareName());
        // TODO Find a cleaner way for fetching square between two pieces (depending on piece movements)
        // TODO Loop on each square to see if one of our piece can go there

        // Check if the piece that is attacking the King can be	captured
        if (m_board.getNextPlayer()->isSquareControledByOpponent(m_lastIsCheckResult->getMySquareName()))
            return false;
    }

    return false;
}

void Player::setKing(Piece *piece)
{
    m_king = piece;
}
