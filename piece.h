/* Copyright (c) (2012) (Gauthier Brion) Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef PIECE_H
#define PIECE_H

#include <QObject>
#include <QStringList>

class Board;
class QDeclarativeItem;

class Piece : public QObject
{
    Q_OBJECT

public:
    enum Type {
        Pawn,
        Rook,
        Knight,
        Bishop,
        Queen,
        King
    };

    enum Color {
        White,
        Black
    };

    explicit Piece(Type type, Color color, const Board &board, QDeclarativeItem *square);
    ~Piece();

    void setSelectedState(bool isSelected);
    bool isAllowedToMove() const;
    QStringList getAllowedDestinations() const;
    void move(QDeclarativeItem *square);
    virtual QStringList getPathTo(const QString &squareName) = 0;

    Type getType() const;
    Color getColor() const;
    QString getMySquareName() const;
    QDeclarativeItem *getDeclarativeItem() const;
    int getMoveCount() const;

signals:
    void clicked();

protected:
    // We will store the last allowed destinations calculated in order to re-use it if possible
    virtual QStringList foundAllowedDestinations() const = 0;
    mutable int m_calculatedMoveCount;
    mutable QStringList m_calculatedAllowedDestinations;

    Type m_type;
    Color m_color;
    int m_moveCount;
    QDeclarativeItem *m_declarativeItem;
    const Board &m_board;
};

#endif // PIECE_H
